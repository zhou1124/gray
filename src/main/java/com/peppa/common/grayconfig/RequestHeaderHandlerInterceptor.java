package com.peppa.common.grayconfig;

import com.peppa.common.util.IpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Enumeration;

public class RequestHeaderHandlerInterceptor implements HandlerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * 进入controller层之前拦截请求
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        try {
            logger.info("----开始进入请求地址拦截----{}",httpServletRequest.getRequestURL());
            Enumeration<String> eunm = httpServletRequest.getHeaders(FeignHeaderRequestInterceptor.HTTP_X_FORWARDED_FOR);
            while (eunm.hasMoreElements()){
                logger.info("----打印header--HTTP_X_FORWARDED_FOR--{}",eunm.nextElement());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            HttpServletRequest request = httpServletRequest;
            Enumeration<String> headerNames = request.getHeaderNames();
            boolean find_xforwardid = false;
            if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();
                    if (name.equals(FeignHeaderRequestInterceptor.HTTP_X_FORWARDED_FOR)) {
                        String values = request.getHeader(name);
                        if (IpUtils.isLocalAddress(values)){
                            values=IpUtils.getIpAddr(request);
                        }
                        ThreadAttributes.setThreadAttribute(FeignHeaderRequestInterceptor.HTTP_X_FORWARDED_FOR,values);
                        logger.info("http 拦截 header:{},{}", name,values);
                        find_xforwardid = true;
                    }
                    //所有带huohua-前缀的，都是自定义头，全部继承下去
                    if (name.startsWith(FeignHeaderRequestInterceptor.HUOHUA_PREFIX)){
                        String values = request.getHeader(name);
                        logger.info("http 拦截 header:{},{}",name, values);
                        ThreadAttributes.setThreadAttribute(name,values);
                    }
                }
            }
            if (!find_xforwardid) {
//                String remoteAddr = request.getRemoteAddr();
                String remoteAddr = IpUtils.getIpAddr(request);
                logger.info("http 增加remoteAddr header:{}", remoteAddr);
                ThreadAttributes.setThreadAttribute(FeignHeaderRequestInterceptor.HTTP_X_FORWARDED_FOR,remoteAddr);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;

    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        ThreadAttributes.remove();
        logger.info("---------------------请求处理结束拦截----------------------------");
        return;
    }

}