package com.peppa.common.grayconfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * threadlocal传递参数的静态方法
 */
public class ThreadAttributes {
    final private static Logger logger = LoggerFactory.getLogger(ThreadAttributes.class);

    private static ThreadLocal<Map<String, Object>> threadAttribues = new ThreadLocal<Map<String, Object>>() {

        protected synchronized Map<String, Object> initialValue() {
//            System.out.println("new ThreadAttributes======================");
            return new HashMap<String, Object>();
        }
    };

    public static Object getThreadAttribute(String name) {

        return threadAttribues.get().get(name);

    }

    public static Object setThreadAttribute(String name, Object value) {

        return threadAttribues.get().put(name, value);

    }
    public static void remove(){
        threadAttribues.remove();
    }
    /**
     * 获得某个header的value
     * @param header_key
     * @return
     */
    public static String getHeaderValue(String header_key){
        String headervalue = (String) ThreadAttributes.getThreadAttribute(FeignHeaderRequestInterceptor.HUOHUA_PREFIX.concat(header_key));
        if (headervalue == null || headervalue.trim().equals("")){
//            logger.debug("headervalue threadlocal :---null!!!");
            return null;
        }
        return headervalue.trim();
    }
}