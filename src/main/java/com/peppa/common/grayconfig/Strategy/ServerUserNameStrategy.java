package com.peppa.common.grayconfig.Strategy;

import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.peppa.common.grayconfig.Strategy.factory.AnnoStrategy;

/**
 * @author ：杜戈
 * @date ：Created in 2019-08-21 17:18
 * @description：服务器端用户名策略，服务器上的策略字符串中为一个正则，如果header中的uname满足这个正则，则可以选择这个服务器
 */
@AnnoStrategy
public class ServerUserNameStrategy extends StrategyAbstract {
    private final String header_key = "uname";
    private final String strategy_key = "gray-name";

    public ServerUserNameStrategy() {
        setName(S_NA);
        setOrder(SORT_S_NA);
    }

    @Override
    public Server getServer(ILoadBalancer balancer){
        return getServerByHeader(balancer,header_key,strategy_key);
    }

    @Override
    public String getName(){
        return name;
    }
}
