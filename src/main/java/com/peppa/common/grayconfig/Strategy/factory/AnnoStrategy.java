package com.peppa.common.grayconfig.Strategy.factory;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ：杜戈
 * @date ：Created in 2019-08-22 20:19
 * @description：自定义策略注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface AnnoStrategy {
}
