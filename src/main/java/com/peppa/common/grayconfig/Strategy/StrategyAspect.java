package com.peppa.common.grayconfig.Strategy;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author ：杜戈
 * @date ：Created in 2019-08-23 20:32
 * @description：策略的切面* com.loongshawn.method.ces..*.
 */
@Component
@Aspect
public class StrategyAspect {
    private final static Logger logger = LoggerFactory.getLogger(StrategyAspect.class);
    @Pointcut("execution(* com.peppa.common.grayconfig.Strategy.Strategy.getServer(..))")
    public void  pointCutStrategy(){
    }

    @Before("pointCutStrategy()")
    public void pointCutGetServerHandler(JoinPoint point) {
        Strategy strategy = (Strategy)point.getTarget();
        logger.debug("=====策略"+strategy.getName()+"启动");
    }
}
