package com.peppa.common.grayconfig.Strategy;

import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

public interface Strategy extends Comparable<Strategy>{
    //策略名称
    String A_IP="A_IP_";
    String A_DEV="A_DEV_";
    String S_IP="S_REGIP_";
    String S_NA="S_REGNA_";
    String S_TAG="S_TAG_";
    String H_SVIP = "H_SVIP_";
    String H_IPS = "H_IPS_";

    //策略tag
    String TAG_WHITELIST = "graywl";
    String TAG_BLACKLIST = "graybl";
    String DEFAULT_STRATEGY = "DEF";

    //策略排序
    int SORT_H_IPS = 1;
    int SORT_H_SVIP = 2;
    int SORT_S_TAG = 3;
    int SORT_S_IP = 4;
    int SORT_S_NA = 5;
    int SORT_A_IP = 6;
    int SORT_A_DEV = 7;

    //分隔符
    String DELIM_ITEM = ",";
    String DELIM_VALUE = ":";

    String getName();
    Server getServer(ILoadBalancer balancer);
    int getOrder();
}
