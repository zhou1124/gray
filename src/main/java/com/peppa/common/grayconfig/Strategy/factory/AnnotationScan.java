package com.peppa.common.grayconfig.Strategy.factory;

import com.peppa.common.grayconfig.Strategy.factory.StrategyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ：杜戈
 * @date ：Created in 2019-08-22 20:45
 * @description：扫描所有策略
 */
@Configuration
public class AnnotationScan{

    @Autowired
    ApplicationContext applicationContext;

    @Bean(name = "strategyFactroy",initMethod="init")
    StrategyFactory strategyFactroy(){
        return new StrategyFactory(applicationContext);
    }

}
